## Installation

1- Clone the repo

`git clone git@gitlab.com:BeGnuLinux/ecmstranslation.git`

2- Copy folder to Modules folder in you laravel project and rename it to "EcmsTranslation" with capital letters

3- Make sure you installed "nwidart/laravel-modules" package

4- Enable you module from modules_statuses.json file in project root

go to 

`http://yourdomain/translations`
